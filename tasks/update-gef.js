"use strict"

module.exports = function(gulp, plugins, options) {

  function gitFetch() {
    gulp.start("git-merge")
  }

  gulp.task("update-gef", function() {
    // see if current user has 'checkedout' the develop branch with no pending commits
    plugins.git.exec({args: "branch -l"}, function(err, stdout) {
      if (err) { throw err } else {
        // split out all available branches in an array
        var output = stdout.replace(/\n/g, ";").replace(/\n/g, "").split(";")
        // filter through branches to find the current branch indicator "*"
        for (var i = 0; i < output.length; i++) {
          if (output[i].indexOf("*") >= 0) {
            // remove the indicator from the current branch string and check if its the develop branch
            var curBranch = output[i].replace(/\*\s/g, "")
            if (curBranch === options.update.developmentBranch) {
              // progress to status check
              gulp.start("update-gef_git-status")
            } else {
              // user isn't using the development branch present them with error
              console.log("ERROR: You need to checkout the develop branch then restart this task `$ gulp update-gef`")
            }
          }
        }
      }
    })
  })

  gulp.task("update-gef_git-status", function() {
    // check to see if the development branch is ahead or behind the remote branch
    plugins.git.status({args: "--porcelain"}, function(err, stdout) {
      if (err) { throw err } else {
        if (stdout.length === 0) {
          // progress to tag merging
          gulp.start("update-gef_test-tag-branch")
        } else {
          // the users local development branch is ahead or behind the remote branch, throw error
          console.log("ERROR: The development branch is ahead or behind the remote branch. Please pull/commit/push as necessary then restart this task `$ gulp update-gef_git-status`")
        }
      }
    })
  })

  gulp.task("update-gef_test-tag-branch", function() {
    // retrieve a list of local git branches
    plugins.git.exec({args: "branch -l"}, function(err, stdout) {
      if (err) { throw err } else {
        // split out all available branches in an array
        var output     = stdout.replace(/\s\s\s/g, ";").replace(/\*\s|\n/g, "").split(";")
        // search array for tag update branch
        var condBranch = output.indexOf(options.update.tagBranch)
        // check to see if branch already exists
        if (condBranch >= 0) {
          // if it does, delete it
          plugins.git.exec({args: "branch -D " + options.update.tagBranch})
        }
        // create the branch
        gulp.start("update-gef_create-tag-branch")
      }
    })
  })

  gulp.task("update-gef_create-tag-branch", function() {
    // create new tag branch
    plugins.git.branch(options.update.tagBranch, function(err) {
      if (err) { throw err } else {
        // if created successfully check it out
        gulp.start("update-gef_checkout-tag-branch")
      }
    })
  })

  gulp.task("update-gef_checkout-tag-branch", function(done) {
    // checkout the tag merge branch if already existing or just created
    plugins.git.checkout(options.update.tagBranch, function(err) {
      if (err) { throw err } else {
        // if the checkout was successful display the current branch
        gulp.start("update-gef_init-update")
      }
    })
  })

  gulp.task("git-checkout-develop", function(done) {
    // checkout the develop branch
    plugins.git.checkout(options.update.developmentBranch, function(err) {
      if (err) { throw err } else {
        // if the checkout was successful merge in the tag merge branch
        console.log("Update successful or no update required. Be sure to double check the development branch to see if merged tag changes need to be committed. Have a nice day! :)")
        done()
      }
    })
  })

  gulp.task("update-gef_init-update", ["update-gef_set-upstream"], function() {
    console.log("Checking for updates...")
    gulp.start("update-gef_fetch-upstream")
  })

  // Checks for upstream repo or sets it if doesn't exist
  gulp.task("update-gef_set-upstream", function(done) {
    // Check the upstreams exists
    plugins.git.exec({args: "remote -v"}, function(err, stdout) {
      if (err) { throw err } else {
        // No errors; Get the upstream value
        var output      = stdout.replace(/\s/g, " ").split(" "),
            upstreamSrc = output[output.indexOf("upstream") + 1]

        if (upstreamSrc !== options.update.source) {
           // add the upstream origin
          plugins.git.addRemote("upstream", options.update.source, function(err) {
            if (err) { throw err } else {
              // No errors
              console.log("Added the new upstream origin...")
              done()
            }
          })
        } else {
          console.log("Correct upstream already set...")
          done()
        }
      }
    })
  })

  // Fetch the latest from the upstream repo
  gulp.task("update-gef_fetch-upstream", function() {
    console.log("Fetching upstream...")
    // Fetch command
    plugins.git.fetch("upstream", "", function(err) {
      if (err) { throw err } else {
        // Do the merge
        gulp.start("update-gef_merge-latest-tag")
      }
    })
  })

  // Merge the latest tag into this repo
  gulp.task("update-gef_merge-latest-tag", function() {
    console.log("Merging...")
    // Get the latest tag
    plugins.git.tag( function (result) {
      // No errors; Get the alatest tag
      var latestTag = result.slice(-2)[0]; // result array returns an empty value as the last index, so we need the -2 to get the most recent
      // Merging
      plugins.git.merge(latestTag, function(err) {
        if (err) {
          console.log("There are GIT conflicts in the options.update.tagBranch branch which case you'll need to manually fix the problems. Once you have committed the changes, run `...` to complete the update.")
          throw err
        } else {
          // No errors, update bower
          gulp.start("update-gef_watch")
        }
      })
    })
  })

  gulp.task("update-gef_watch", ["update-gef_update-bower"], function() {
     // deploy the code
    gulp.start('watch')
  })

  gulp.task("update-gef_update-bower", function() {
    return plugins.bower();
  })
}
