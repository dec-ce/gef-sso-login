"use strict"

var configuration = {
  name : "GEF Master Template",
  components : {

  	  "feedbackTrigger" : {
      "selector" : ".gef-feedback-trigger",
      "script" : "components/feedbackTrigger"
    },

  },
  requireConfig : {

  }
}

// Update RequireJS config options
require.config(configuration.requireConfig)

export default configuration